import altair as alt
import pandas as pd
import numpy as np
from vega_datasets import data
import json
import pandas as pd
import numpy as np
import streamlit as st
import regex as re

# Data Sources
states = alt.topo_feature(data.us_10m.url, 'states')
data_file = open("yelp_academic_dataset_business.json", encoding='UTF-8')
data = []
for line in data_file:
  data.append(json.loads(line))
bus_data = pd.DataFrame(data)
data_file.close()

#drop businesses with no hours posted
bus_data = bus_data[bus_data['hours'].notnull()]

def avg_hours(row):
  #finds the average business hours for a single business
  pattern = '[:-]'
  open_hour = 0
  open_min = 0
  closed_hour = 0
  closed_min = 0
  #print(row.hours)
  #print(row)
  for k,v in row.items():
    stuff = re.split(pattern,v)
    #print(stuff)
    open_hour+=int(stuff[0])
    open_min+=int(stuff[1])
    closed_hour+=int(stuff[2])
    closed_min+=int(stuff[3])
  n = len(row.items())
  #string = str(open_hour//n)+':'+str(open_min//n) + '-' + str(closed_hour//n)+':'+str(closed_min//n)
  open_min = (open_hour*60+open_min)/n
  closed_min = (closed_hour*60+closed_min)/n
  return open_min,closed_min

bus_data['open_min'],bus_data['closed_min'] = zip(*bus_data['hours'].map(avg_hours))
bus_count_state = bus_data.groupby(by='state').mean()
sentiment = pd.read_csv("final3.csv")

bus_count_state = bus_count_state.merge(sentiment,left_on='state',right_on='state')

#dropping ABE ON BC
bus_count_state = bus_count_state.drop([0,3,21])

def state_to_id(row):
  conv = {'AL':1, 'AK': 2, 'AZ':4, 'AR':5 , 'CA':6, 'CO':8, 'CT':9, 'DE':10, 'DC':11, 'FL':11, 'GA':13, 'HI':15, 'ID':16, 'IL':17,'IN':18, 'IA':19, 'KS':20, 'KY':21, 
          'LA':22, 'ME':23, 'MD':24, 'MA':25, 'MI':26, 'MN':27, 'MS':28, 'MO':29, 'MT':30, 'NE':31, 'NV':32, 'NH':33, 'NJ':34,'NM':35,'NY':36,'NC':37,'ND':38,'OH':39,
          'OK':40,'OR':41,'PA':42,'RI':44, 'SC':45,'SD':46,'TN':47,'TX':48,'UT':49,'VT':50,'VA':51,'WA':53,'WV':54,'WI':55,'WY':56,'PR':72}
  #print(row)
  return conv[row]

bus_count_state = bus_count_state.reset_index()
bus_count_state['state_id']= bus_count_state['state'].apply(state_to_id)

bus_count_state = bus_count_state.set_index('state_id')
bus_count_state = bus_count_state.reset_index()

def min_time(minutes):
  minutes = int(minutes)
  hour = minutes//60
  min = minutes-hour*60
  hour = str(hour).zfill(2)
  min = str(min).zfill(2)
  return str(hour) +":"+str(min)

bus_count_state['open_min'] = bus_count_state['open_min'].apply(min_time)
bus_count_state['closed_min'] = bus_count_state['closed_min'].apply(min_time)

selections = alt.selection_multi(fields=['state_id'])
color2 = alt.condition(selections,'state_id:N',alt.value('black'),legend=None)
foreground = alt.Chart(states).mark_geoshape().encode(
    #color='business_id:Q',
    color=color2,
    tooltip=['state:N','state_id:N']
).transform_lookup(
    lookup='id',
    from_=alt.LookupData(bus_count_state, 'state_id', ['business_id','state_id','state','open_min','closed_min'])
).project(
    type='albersUsa'
).properties(
    width=600,
    height=400
).add_selection(selections)


ranked_text = alt.Chart(bus_count_state).mark_text().encode(
    y=alt.Y('row_number:O',axis=None)
).transform_window(
    row_number='row_number()'
).transform_filter(
    selections
).transform_window(
    rank='rank(row_number)'
).transform_filter(
    alt.datum.rank<20
)
state_name = ranked_text.encode(text='state:N').properties(title='State',width=10,)
open_time = ranked_text.encode(text='open_min:N').properties(title='Opening Time',width=12)
close_time = ranked_text.encode(text='closed_min:N').properties(title='Closing Time',width=12)
star_rating = ranked_text.encode(text='stars_y:Q').properties(title='Average Star Rating',width=12)
sent_rating = ranked_text.encode(text='sentiment:Q').properties(title='Average Sentiment',width=12)
text = alt.hconcat(state_name,open_time,close_time,star_rating,sent_rating)

map = alt.hconcat(
    foreground,
    text
).resolve_legend(
    color="independent"
).configure_view(
    strokeWidth=0
).configure_legend(disable=False)

#foreground | state_name |open_time| close_time | star_rating| sent_rating  
map
